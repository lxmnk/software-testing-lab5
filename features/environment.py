import os

from selenium import webdriver


def before_all(context):
    CHROME = 'Chrome'
    FIREFOX = 'Firefox'
    sym = '-'
    sym_mul = 8
    browser = os.environ.get('BROWSER', 'Chrome')
    if browser == CHROME:
        print('{} {} {}'.format(sym * sym_mul, CHROME, sym * sym_mul))
        context.driver = webdriver.Remote('http://localhost:4445/wd/hub',
                webdriver.DesiredCapabilities.CHROME.copy())
    if browser == FIREFOX:
        print('{} {} {}'.format(sym * sym_mul, FIREFOX, sym * sym_mul))
        context.driver = webdriver.Remote('http://localhost:4444/wd/hub',
                webdriver.DesiredCapabilities.FIREFOX.copy())


def after_all(context):
    context.driver.quit()

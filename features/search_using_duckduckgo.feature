Feature: search something using DuckDuckGo

	Scenario Outline: search ArchLinux, GitHub, Vim
		Given I open DuckDuckGo website
		When I input <request> in search form and submit it
		Then it should be <link> in search results

	Examples:
		|    request  |           link  |
		|  ArchLinux  |  archlinux.org  |
		|     GitHub  |     github.com  |
		|        Vim  |        vim.org  |

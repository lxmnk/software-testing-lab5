Feature: register fake user in Yandex

	Scenario Outline: ^ see above
		Given I open Yandex.Passport website
		When I input <first_name> in first name field
		And I input <last_name> in last name field
		And I input <login> in login field
		And I input <passwd> in password field
		And I input <passwd> in password confirm field
		And I press no phone number button
		And I input <fav_musician> in favourite musician field
		And I pressed register button
		Then it should be captcha error

	Examples:
		|  first_name  |  last_name  |         login  |   passwd  |  fav_musician  |
		|        Ivan  |     Ivanov  |  keklolivanov  |  ^81jaAS  |         Ariya  |
		|     Arsenii  |    Akafeev  |      arsefeev  |  Kjasi!k  |   Linkin Park  |

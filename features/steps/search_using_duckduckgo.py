from behave import *
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


@given('I open DuckDuckGo website')
def step_impl(context):
    context.driver.get('https://start.duckduckgo.com/')


@when('I input {request} in search form and submit it')
def step_impl(context, request):
    input_id = 'search_form_input_homepage'
    try:
        input_element = WebDriverWait(context.driver, 10).until(
                EC.presence_of_element_located((By.ID, input_id)))
    except TimeoutException:
        exit(1)
    input_element.send_keys(request)
    input_element.submit()


@then('it should be {link} in search results')
def step_impl(context, link):
    try:
        link_elements = WebDriverWait(context.driver, 10).until(
                EC.presence_of_all_elements_located((By.CLASS_NAME, 'result')))
    except TimeoutException:
        exit(1)
    links = [x.get_attribute('data-domain') for x in link_elements]
    assert link in links

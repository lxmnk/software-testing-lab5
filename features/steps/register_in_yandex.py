from behave import *
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


@given('I open Yandex.Passport website')
def step_impl(context):
    context.driver.get('https://passport.yandex.ru/registration/')


@when('I input {first_name} in first name field')
def step_impl(context, first_name):
    input_id = 'firstname'
    input_text(context, first_name, (By.ID, input_id))


@when('I input {last_name} in last name field')
def step_impl(context, last_name):
    input_id = 'lastname'
    input_text(context, last_name, (By.ID, input_id))


@when('I input {login} in login field')
def step_impl(context, login):
    input_id = 'login'
    input_text(context, login, (By.ID, input_id))


@when('I input {passwd} in password field')
def step_impl(context, passwd):
    input_id = 'password'
    input_text(context, passwd, (By.ID, input_id))


@when('I input {passwd} in password confirm field')
def step_impl(context, passwd):
    input_id = 'password_confirm'
    input_text(context, passwd, (By.ID, input_id))


@when('I press no phone number button')
def step_impl(context):
    context.driver.find_element_by_class_name('link_has-no-phone').click()


@when('I input {fav_musician} in favourite musician field')
def step_impl(context, fav_musician):
    input_id = 'hint_answer'
    input_text(context, fav_musician, (By.ID, input_id))


@when('I pressed register button')
def step_impl(context):
    captcha = context.driver.find_element_by_id('captcha')
    captcha.click()
    captcha.submit()
    

@then('it should be captcha error')
def step_impl(context):
    dest = (By.CLASS_NAME, 'form__popup-error')
    text = 'Необходимо ввести символы'
    try:
        WebDriverWait(context.driver, 10).until(
                EC.text_to_be_present_in_element(dest, text))
    except TimeoutException:
        exit(1)


def input_text(context, text, dest):
    try:
        input_element = WebDriverWait(context.driver, 10).until(
                EC.presence_of_element_located(dest))
    except TimeoutException:
        exit(1)
    input_element.send_keys(text)
